# Smart Alert
### Sistema de predição e notificação para zonas de inundação 

----

> UNICID - Universidade Cidade de São Paulo
>
> Autores:
> 
> Evandro Barbosa - Fernando Tandu - Italo Tailan - Johannes Merschbacher - Pedro Gandhi - Yuri Marin

----
#### Utilização:

    git clone git@gitlab.com:vandoenterprise/web-scraping.git
    cd web-scraping
    npm install
    node index.js
