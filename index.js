const puppeteer = require('puppeteer');
const axios = require('axios');
const firebase = require('firebase');

// Arquivo de Configuração do Firebase
var config = {
    apiKey: "AIzaSyDJgqGxxF8Qg_d5zFm4G4TnH0_jPcCOQs0",
    authDomain: "smart-alert-db.firebaseapp.com",
    databaseURL: "https://smart-alert-db.firebaseio.com",
    projectId: "smart-alert-db",
    storageBucket: "smart-alert-db.appspot.com",
    messagingSenderId: "1077769596113"
};

// Abre conexão com o Firebase
firebase.initializeApp(config);

/**
 * Salva a última mensagem no banco de dados
 * @param {String} mensagem
 */
function salvaUltimaMensagem(mensagem) {
    return firebase.database()
        .ref('notificacao')
        .set({
            ultimaMensagem: mensagem
        });
}

/**
 * Retorna a URL para a requisição
 */
function retornaURL() {
    // Armazena o valor do parametro se passado
    let parametros = process.argv.slice(2);

    // Verifica se existe algum parametro
    if (parametros.length != 0) {
        // Faz o tratamento da data e armazena em uma variável
        let data = parametros.toString().replace(/\//g, '%2F');

        // Retorna a url com o parametro da data
        return `https://www.cgesp.org/v3/alagamentos.jsp?dataBusca=${data}&enviaBusca=Buscar`;
    } else {
        // Retorna a url
        return 'https://www.cgesp.org/v3/alagamentos.jsp';
    }
}

/**
 * Envia a Mensagem via Telegram
 * @param {String} mensagem
 */
function enviaMensagem(mensagem) {
    // Armazena os dados da API do Telegram, com id do chat, canal de transmissão e mensagem
    const api = `https://api.telegram.org/bot729212312:AAGNtGJhU_Grcu1c7yYW2JlFFoYX0V8CYMM/sendMessage?chat_id=@smartAlertTCC&text=${mensagem}`;

    // Armazena a referencia do campo ultimaMensagem que existe dentro do banco de Dados
    let referenciaChave = firebase.database().ref("notificacao/ultimaMensagem");

    // Executa uma consulta de leitura para o campo ultimaMensagem
    referenciaChave.once("value", snapshot => {

        // Verifica se o campo trazido do banco de dados é igual a mensagem a ser enviada
        if (snapshot.val() != mensagem) {

            // Executa uma função assíncrona de requisição com o método GET, convertendo os dados da variável 'api' para UTF-8
            axios.get(unescape(encodeURIComponent(api)))
            .then(() => {

                // Exibe no console a resposta com o status positivo
                console.log('Mensagem Enviada');

                // Salva os dados da última mensagem enviada
                salvaUltimaMensagem(mensagem)
                .then(() => {
                    // Exibe no console mensagem de sucesso ao salvar dados
                    console.log('Última Mensagem Salva no Banco de Dados');

                    // Encerra a conexão com o banco de dados
                    firebase.database().goOffline();
                });
            })
            .catch(() => {
                // Exibe no console a resposta com o status de não encontrado
                console.log('Nenhuma ocorrência foi identificada');

                // Encerra a conexão com o banco de dados
                firebase.database().goOffline();
            });
        } else {
            // Exibe no console que a mensagem já foi enviada
            console.log('Mensagem já enviada');

            // Encerra a conexão com o banco de dados
            firebase.database().goOffline();
        }
    });
}

// O Puppeter é iniciado com uma função assíncrona para abrir o navegador
puppeteer.launch().then(async (navegador) => {
    let pagina = null;

    // Bloco responsável por retornar os dados da página ou os erros
    try {
        // Armazena uma página em branco na variável page
        pagina = await navegador.newPage();

        // Executa a função de ir para o link passado na função retornaURL()
        await pagina.goto(retornaURL());
    } catch (erro) {
        // Exibe mensagem de erro no terminal
        return console.log(erro);
    }

    // Armazena o resultado dos dados da busca por elementos dentro da página
    const enchentes = await pagina.$$eval('.tb-pontos-de-alagamentos', (nodes) => {
        // Retorna os dados tratados dado da matriz
        return nodes.map((node) => {
            // Armazena o valor tratado do elemento na variavel seguinte
            const bairro = node.querySelector('.bairro').innerText.trim();

            // Armazena o valor tratado do elemento na variavel seguinte
            const status = node.querySelector('li[title]').getAttribute('title').replace('Inativo ', '');

            // Armazena os elementos na variavel seguinte
            const elementosLocais = node.querySelectorAll('.arial-descr-alag');
            // Armazena os valores tratados dos elementos na variavel seguinte
            const locais = [...elementosLocais].map((elemento) => {
                [, horario, local] = elemento.innerText.match('(.*)\n(.*)');

                return {
                    local
                };
            });

            // Armazena o valor tratado do elemento na variavel seguinte
            const pontos = node.querySelector('.total-pts').innerText;

            // Retorna o resultado dos dados da busca por elementos dentro da página
            return {
                bairro: bairro,
                status: status,
                locais,
                pontos: pontos,
            };
        });
    });

    // Armazena os dados da mensagem na variável seguinte
    const mensagem = enchentes.reduce((mensagem, enchente) => {

        // Verifica se o valor retornado é vázio
        if(mensagem !== '') {
            // Adiciona duas quebras de linha na mensagem
            mensagem += '\n\n';
        }

        // Adiciona na mensagem os dados do bairro e da quantidade de pontos de enchentes
        mensagem += `BAIRRO: ${ enchente.bairro } - ${enchente.pontos}\n`;

        // Adiciona na mensagem o status da enchente
        mensagem += `STATUS: ${ enchente.status }\n`;

        // Adiciona na variável os dados de locais e horarios da enchente
        const locais = enchente.locais.reduce((locais, localizacao) => {
            locais += `${localizacao.local}\n`;

            return locais;
        }, 'LOCAIS:\n');

        // Adiciona na mensagem os dados de locais e horarios da enchente
        mensagem += `${locais}`;

        // Retorna a mensagem
        return mensagem;
    }, '');

    // Fecha o navegador
    await navegador.close();


    // Exibe a mensagem no terminal
    console.log(mensagem);

    // chama a função enviaMensagem e passa a mensagem por parametro
    enviaMensagem(mensagem);
});
